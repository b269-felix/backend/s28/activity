// Insert room collections
db.users.insertOne({
	name: "single",
	accomodates: 2,
	price: 1000,
	description: "A simple room with all the basic necessities"
	roomsAvailable: 10,
	isAvailable: "false"
});

// Inserting multiple rooms
db.users.insertMany([
	{
		name: "double",
		accomodates: 3,
		price: 2000,
		description: "A room fit for a small family going on a vacation",
		roomsAvailable: 5,
		isAvailable: "false"
	},
	{
        name: "queen",
        accomodates: 4,
        price: 4000,
        description: "A room with a queen sized bed perfect for a simple getaway",
        roomsAvailable: 15,
        isAvailable: "false"
	}
]);



// searchin for a room with name double
db.users.find({name: "double"});

// Updating the queen room and setting the available rooms to 0
db.users.updateOne({roomsAvailable: 15}, {$set: {roomsAvailable: 0}});

// deleting rooms with 0 available value
db.users.deleteMany(
	{roomsAvailable: 0}
);